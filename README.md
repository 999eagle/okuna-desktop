# okuna-desktop

This is a desktop app for Okuna. As the app itself is written using flutter, `okuna-desktop` uses the flutter engine library to run the app on desktop.

Binary releases will be uploaded [here](https://gitlab.com/999eagle/okuna-desktop/releases), most likely shortly after a new version of the app itself is published. The version number of `okuna-desktop` will always match the app version.

## Issues

Some issues are specific to the desktop version and not necessarily to the app itself. If you find any issues specific to this desktop version, please report them here by opening a new issue. Alternatively, you can find me in the Okuna Slack channel as well.

If you report issues, please include a log file and screenshots. For some issues it's helpful to output more information. You can run `okuna-desktop` in the terminal/console with `-v` or `-vv` to output more log messages. Log files can be found here:

* v0.0.50-1 to current
	* Linux: `~/.local/share/okuna/logs/latest.log`
	* macOS: `~/Library/Logs/OkunaDesktop/latest.log`
	* Windows: `%localappdata%\okuna\logs\latest.log`
* v0.0.49 to v0.0.50
	* Linux: `~/.local/share/okuna/okuna-desktop.log`
	* macOS: `~/Library/Logs/OkunaDesktop/okuna-desktop.log`
	* Windows: `%localappdata%\okuna\okuna-desktop.log`
* v0.0.44 to v0.0.45:
	* Linux: `~/.local/share/openspace/openspace-desktop.log`
	* macOS: `~/Library/Logs/OpenspaceDesktop/openspace-desktop.log`
	* Windows: `%localappdata%\openspace\openspace-desktop.log`
* v0.0.34-6-rc1 to v0.0.43-2:
	* Linux: `~/.local/share/openbook/openbook-desktop.log`
	* macOS: `~/Library/Logs/OpenbookDesktop/openbook-desktop.log`
	* Windows: `%localappdata%\openbook\openbook-desktop.log`
* v0.0.34-4 to v0.0.34-5:
	* Linux: `~/.local/share/openbook-desktop/openbook-desktop.log`
	* macOS: `~/Library/Logs/OpenbookDesktop/openbook-desktop.log`
	* Windows: `%localappdata%\openbook-desktop\openbook-desktop.log`
* v0.0.34-3 and lower:
	* In the same directory where the binary is

## Settings

This is the default settings file. All available settings for `okuna-desktop` are explained in the file itself.
```toml
# Proxy-related settings. You should keep these at their default values unless you have connection issues.
[proxy]
# Change `enable_auto_config` to `true` to enable proxy auto configuration. This potentially downloads and runs Javascript
# code on your pc if enabled.
enable_auto_config = false

# Uncomment the next line to force the specified proxy to be used, no matter your OS configuration.
#force_proxy = "http://proxy.example.com:8080/"
```

You can find the settings file here:

* vnext to current:
    * Linux: `~/.config/okuna/settings.toml`
    * macOS: `~/Library/Preferences/OkunaDesktop/settings.toml`
    * Windows: `%appdata%/okuna/settings.toml`
    
## Binary releases

Binary releases can be downloaded from the [Releases](https://gitlab.com/999eagle/okuna-desktop/-/releases) page in this repo. Every CI pipeline also finishes with release builds for all three platforms, so if you want you can download unreleased versions as well. Finished pipelines on `master` should be stable to use. An AppImage for Linux is also available for releases and every pipeline on `master`.

Packages are also available:

* **Arch Linux**: `okuna-desktop-bin` is available in the AUR.
* **macOS**: There's a homebrew tap available made by @stranljip. You can install it with `brew tap stranljip/okuna-desktop  && brew cask install okuna-desktop`.

### Running releases

Binaries are released in a zipped file. Please download the release for your platform and extract all files from the zip to another location. Then follow the step(s) below for your platform.

For Linux there is additionally an AppImage available you can download. This should not be extracted.

#### Linux

* If you downloaded a binary release:
    * Install `gnutls`, `openssl`, `x264` and `sqlite` using your package manager (names may vary depending on distribution)
    * Run `okuna-desktop.sh`
* If you downloaded the AppImage:
    * Run `chmod +x OkunaDesktop-x86_64.AppImage`
    * Execute the AppImage

#### macOS

* Double click on `OkunaDesktop` in Finder
* Confirm that you want to run it in System Preferences > Security & Privacy

#### Windows

* Install the Visual C++ Redistributable included in the download or from here: https://aka.ms/vs/16/release/vc_redist.x64.exe
* Run `okuna-desktop.exe`

## Building

On all platforms you will need to have the [Flutter SDK](https://flutter.dev/docs/development/tools/sdk/releases) and [Rust](https://www.rust-lang.org/tools/install) installed. Currently, due to upstream requirements of `okuna-app`, using the flutter master channel is mandatory.

Be prepared for long build times on slower machines the first time. Okuna-desktop depends on ffmpeg and (if enabled) mozjs and both libraries are compiled from source during compilation of okuna-desktop.

### Dependencies on Linux

* Install these packages: `cmake libxrandr-dev libxinerama-dev libxcursor-dev libgl1-mesa-dev libxi-dev libgtk-3-dev clang-7 llvm-7-dev libclang-7-dev yasm libssl-dev libx264-dev`
* If you compile with proxy support (enabled by default) you'll also need to install `autoconf2.13 python2`

### Dependencies on macOS

* Install these packages with brew: `x264`
* Make sure to have XCode and the XCode Command Line Tools installed
* If you compile with proxy support (enabled by default) you'll also need to install `autoconf@2.13 python2 clang yasm` with `brew`
* Set these environment variables:
    - `LDFLAGS="-L/usr/local/opt/openssl/lib"`
    - `CPPFLAGS="-I/usr/local/opt/openssl/include"`

### Dependencies on Windows

* Build Tools for Visual Studio 2019 or a full installation of Visual Studio 2019 (older versions may work as well) with the C++ development workload
* [msys2](https://www.msys2.org/) with `nasm` installed
* [libx264](https://www.videolan.org/developers/x264.html). Most likely you'll have to compile from source using msys
* Compile in a Visual Studio Developer Shell or another shell where you have set up the Visual Studio environment variables for x64 development
* Add your x264 library directory to the `LIB` environment variable
* Add your x264 include directory to the `INCLUDE` environment variable
* Add `C:\msys64\usr\bin` to your `PATH`
* Make sure that the `link.exe` used is *not* the one from msys
* Set the `CFLAGS` environment variable to `-DWIN32_LEAN_AND_MEAN`

#### Compiling with proxy support

* Install the Mozilla Build Tools and Clang (64 bit)
* Make sure you install these additional Workloads for Visual Studio:
    - Windows 10 SDK at least 10.0.17134.0
    - C++ ATL for v142 build tools (x86 and x64)
* Set these environment variables:
    - `MOZTOOLS_PATH=C:\mozilla-build\msys\bin;C:\mozilla-build\bin`
    - `AUTOCONF=C:\mozilla-build\msys\local\bin\autoconf-2.13`
    - `NATIVE_WIN32_PYTHON=C:\mozilla-build\python\python2.7.exe`
    - `LIBCLANG_PATH=C:\Program Files\LLVM\lib`

### Running

I'm developing on Linux, but these instructions should work on macOS as well. On Windows, replace `export` with `set`, but I don't know if there's an equivalent to `patch`.

```bash
# Check out okuna-app submodule
git submodule update --init

# Patch okuna-app
cd okuna-app
patch -uNp1 -i ../releasing/binaries/emoji-fix.patch
patch -uNp1 -i ../releasing/binaries/keyboard-fix.patch
patch -uNp1 -i ../releasing/binaries/about.patch
cp ../releasing/binaries/TwemojiAndroid.ttf assets/fonts/Twemoji.ttf

# Create .env.json (see okuna-app repo or the .gitlab-ci.yml file)

# Build app
flutter build bundle

cd ..
# Replace this value with the hash of the flutter engine you're using, this is for the current (2019-10-25) flutter master
export FLUTTER_ENGINE_VERSION="8d6b74aee6fb81e4cb8c4cce43a4755f89f6d9d7"
# Run okuna-desktop
cargo run
```
