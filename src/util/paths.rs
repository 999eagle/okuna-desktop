use std::{env, fs, path::PathBuf};

use log::warn;

use lazy_static::lazy_static;

lazy_static! {
    static ref CARGO_DIR: Option<PathBuf> = env::var("CARGO_MANIFEST_DIR")
        .ok()
        .map(|dir| PathBuf::from(&dir));
    static ref RESOURCE_DIR: PathBuf = CARGO_DIR.clone().unwrap_or_else(|| {
        if cfg!(target_os = "macos") {
            env::current_exe()
                .expect("Cannot get application dir")
                .parent()
                .expect("Cannot get application dir")
                .parent()
                .expect("Cannot get application dir")
                .join("Resources")
                .to_path_buf()
        } else {
            env::current_exe()
                .expect("Cannot get application dir")
                .parent()
                .expect("Cannot get application dir")
                .to_path_buf()
        }
    });
    static ref ASSETS_DIR: PathBuf = RESOURCE_DIR.clone().join("assets");
}

fn get_okuna_dir(dir: Option<PathBuf>, subdir: bool) -> Option<PathBuf> {
    match dir {
        Some(dir) => {
            let dir = if subdir { dir.join("okuna") } else { dir };
            match fs::create_dir_all(&dir) {
                Ok(_) => Some(dir),
                Err(e) => {
                    warn!(
                        "Failed to create directory {}. Error: {}",
                        dir.to_string_lossy(),
                        e
                    );
                    None
                }
            }
        }
        None => None,
    }
}

pub fn get_data_dir() -> Option<PathBuf> {
    get_okuna_dir(dirs::data_dir(), true)
}

pub fn get_home_dir() -> Option<PathBuf> {
    get_okuna_dir(dirs::data_dir(), false)
}

pub fn get_cache_dir() -> Option<PathBuf> {
    get_okuna_dir(dirs::cache_dir(), true)
}

pub fn get_res_dir() -> PathBuf {
    RESOURCE_DIR.clone()
}

pub fn get_assets_dir() -> PathBuf {
    ASSETS_DIR.clone()
}

pub fn get_flutter_paths() -> (PathBuf, PathBuf) {
    let res_dir = get_res_dir();
    match *CARGO_DIR {
        None => (res_dir.join("flutter_assets"), res_dir.join("icudtl.dat")),
        Some(_) => (
            res_dir
                .join("okuna-app")
                .join("build")
                .join("flutter_assets"),
            res_dir
                .join("releasing")
                .join("binaries")
                .join("icudtl.dat"),
        ),
    }
}

pub fn get_settings_dir() -> PathBuf {
    let dir = dirs::config_dir().expect("failed to get settings dir");
    let dir = if cfg!(target_os = "macos") {
        dir.join("OkunaDesktop")
    } else {
        dir.join("okuna")
    };
    fs::create_dir_all(&dir).expect("failed to create settings dir");
    dir
}

pub fn get_settings_file_path() -> PathBuf {
    get_settings_dir().join("settings.toml")
}
