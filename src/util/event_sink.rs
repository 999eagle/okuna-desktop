use std::sync::{Arc, Mutex};

use flutter_engine::{codec::Value, error::MethodCallError, RuntimeData};

#[derive(Clone)]
enum Event {
    Success(Value),
    Error(String, String, Value),
}

enum EventSinkState {
    Listening(RuntimeData),
    Canceled(Arc<Mutex<Vec<Event>>>),
}

pub struct EventSink {
    channel_name: String,
    state: EventSinkState,
}

impl EventSink {
    pub fn new(channel_name: String) -> Self {
        Self {
            channel_name,
            state: EventSinkState::Canceled(Arc::new(Mutex::new(Vec::new()))),
        }
    }

    pub fn start_listening(&mut self, rt: RuntimeData) -> Result<(), MethodCallError> {
        match &self.state {
            EventSinkState::Listening(_) => return Err(MethodCallError::UnspecifiedError),
            EventSinkState::Canceled(deferred) => {
                let mut messages = deferred.lock().unwrap();
                if !messages.is_empty() {
                    let messages_box = Box::new(messages.clone());
                    messages.clear();
                    rt.with_channel(&self.channel_name, move |channel| {
                        if let Some(channel) = channel.try_as_method_channel() {
                            for e in messages_box.iter() {
                                match e {
                                    Event::Error(code, message, details) => {
                                        channel.send_error_event(code, message, details)
                                    }
                                    Event::Success(value) => channel.send_success_event(value),
                                }
                            }
                        }
                    })?;
                }
            }
        }

        self.state = EventSinkState::Listening(rt);

        Ok(())
    }

    pub fn stop_listening(&mut self) -> Result<(), MethodCallError> {
        match &self.state {
            EventSinkState::Listening(_) => {}
            EventSinkState::Canceled(_) => return Err(MethodCallError::UnspecifiedError),
        }

        self.state = EventSinkState::Canceled(Arc::new(Mutex::new(Vec::new())));

        Ok(())
    }

    pub fn success(&self, value: Value) -> Result<(), MethodCallError> {
        match &self.state {
            EventSinkState::Listening(rt) => {
                Ok(rt.with_channel(&self.channel_name, move |channel| {
                    if let Some(channel) = channel.try_as_method_channel() {
                        channel.send_success_event(&value);
                    }
                })?)
            }
            EventSinkState::Canceled(deferred) => {
                let mut values = deferred.lock().unwrap();
                values.push(Event::Success(value));
                Ok(())
            }
        }
    }

    pub fn error(
        &self,
        code: String,
        message: String,
        details: Value,
    ) -> Result<(), MethodCallError> {
        match &self.state {
            EventSinkState::Listening(rt) => {
                Ok(rt.with_channel(&self.channel_name, move |channel| {
                    if let Some(channel) = channel.try_as_method_channel() {
                        channel.send_error_event(code.as_str(), message.as_str(), &details);
                    }
                })?)
            }
            EventSinkState::Canceled(deferred) => {
                let mut values = deferred.lock().unwrap();
                values.push(Event::Error(code, message, details));
                Ok(())
            }
        }
    }
}
