use std::{
    fs::{self, File},
    path::PathBuf,
};

use flutter_engine::plugins::prelude::*;
use image::{DynamicImage, GenericImageView, ImageOutputFormat};
use log::debug;
use serde_repr::{Deserialize_repr, Serialize_repr};

use crate::util::{ffmpeg::FFMPEG, paths};

const PLUGIN_NAME: &str = module_path!();
pub const CHANNEL_NAME: &str = "video_thumbnail";

pub struct VideoThumbnailPlugin {
    handler: Arc<RwLock<Handler>>,
}

struct Handler;

impl Plugin for VideoThumbnailPlugin {
    fn plugin_name() -> &'static str {
        PLUGIN_NAME
    }

    fn init_channels(&mut self, registrar: &mut ChannelRegistrar) {
        let method_handler = Arc::downgrade(&self.handler);
        registrar.register_channel(StandardMethodChannel::new(CHANNEL_NAME, method_handler));
    }
}

impl VideoThumbnailPlugin {
    pub fn new() -> Self {
        Self {
            handler: Arc::new(RwLock::new(Handler)),
        }
    }
}

impl Handler {
    fn get_thumbnail(
        &self,
        video_path: &str,
        target_size: i32,
    ) -> Result<DynamicImage, MethodCallError> {
        let temp_path = paths::get_cache_dir().unwrap().join("video_thumbnail.png");
        if temp_path.exists() {
            fs::remove_file(&temp_path).map_err(MethodCallError::from_error)?;
        }
        let output = FFMPEG
            .run_ffmpeg(vec![
                "-i",
                video_path,
                "-ss",
                "00:00:00.000",
                "-vframes",
                "1",
                &temp_path.to_string_lossy(),
            ])
            .map_err(MethodCallError::from_error)?;
        if !output.status.success() {
            return Err(MethodCallError::CustomError {
                code: "ffmpeg failed".into(),
                message: "FFmpeg failed to execute".into(),
                details: Value::Null,
            });
        }

        let mut img = image::open(&temp_path).map_err(MethodCallError::from_error)?;
        let _ = fs::remove_file(&temp_path);

        if target_size != 0 {
            let w = img.width() as f32;
            let h = img.height() as f32;
            let scale = target_size as f32 / w.max(h);
            let w = (w * scale) as u32;
            let h = (h * scale) as u32;
            img = img.resize(w, h, image::FilterType::Lanczos3);
        }

        Ok(img)
    }

    fn file(&self, args: FileArgs) -> Result<Value, MethodCallError> {
        let img = self.get_thumbnail(args.video.as_str(), args.maxhow)?;
        let (format, ext) = match args.format {
            ImageFormat::PNG => (ImageOutputFormat::PNG, "png"),
            ImageFormat::JPEG => (ImageOutputFormat::JPEG(args.quality as u8), "jpg"),
            ImageFormat::WEBP => (ImageOutputFormat::Unsupported("webp".into()), "webp"),
        };
        let path = PathBuf::from(args.path);
        let path = if path.extension().unwrap().to_string_lossy() == ext {
            path
        } else {
            path.with_extension(ext)
        };
        let mut file = File::create(&path).map_err(MethodCallError::from_error)?;
        img.write_to(&mut file, format)
            .map_err(MethodCallError::from_error)?;
        Ok(Value::String(path.to_string_lossy().into()))
    }

    fn data(&self, args: DataArgs) -> Result<Value, MethodCallError> {
        let img = self.get_thumbnail(args.video.as_str(), args.maxhow)?;
        let format = match args.format {
            ImageFormat::PNG => ImageOutputFormat::PNG,
            ImageFormat::JPEG => ImageOutputFormat::JPEG(args.quality as u8),
            ImageFormat::WEBP => ImageOutputFormat::Unsupported("webp".into()),
        };
        let mut buffer = Vec::new();
        img.write_to(&mut buffer, format)
            .map_err(MethodCallError::from_error)?;
        Ok(Value::U8List(buffer))
    }
}

impl MethodCallHandler for Handler {
    fn on_method_call(
        &mut self,
        call: MethodCall,
        _: RuntimeData,
    ) -> Result<Value, MethodCallError> {
        debug!(
            "Got method call {} with args: {}",
            call.method,
            super::debug_print_args(&call.args)
        );

        match call.method.as_str() {
            "file" => {
                let args = from_value(&call.args)?;
                self.file(args)
            }
            "data" => {
                let args = from_value(&call.args)?;
                self.data(args)
            }
            _ => Err(MethodCallError::NotImplemented),
        }
    }
}

#[derive(Serialize_repr, Deserialize_repr)]
#[repr(i32)]
pub enum ImageFormat {
    JPEG = 0,
    PNG = 1,
    WEBP = 2,
}

#[derive(Serialize, Deserialize)]
struct FileArgs {
    video: String,
    path: String,
    format: ImageFormat,
    maxhow: i32,
    quality: i32,
}

#[derive(Serialize, Deserialize)]
struct DataArgs {
    video: String,
    format: ImageFormat,
    maxhow: i32,
    quality: i32,
}
