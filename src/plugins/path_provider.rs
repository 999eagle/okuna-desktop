use std::path::PathBuf;

use crate::util::paths;

use flutter_engine::plugins::prelude::*;
use log::debug;

pub const PLUGIN_NAME: &str = module_path!();
pub const CHANNEL_NAME: &str = "plugins.flutter.io/path_provider";

pub struct PathProviderPlugin {
    handler: Arc<RwLock<Handler>>,
}

struct Handler;

impl Plugin for PathProviderPlugin {
    fn plugin_name() -> &'static str {
        PLUGIN_NAME
    }

    fn init_channels(&mut self, registrar: &mut ChannelRegistrar) {
        let method_handler = Arc::downgrade(&self.handler);
        registrar.register_channel(StandardMethodChannel::new(CHANNEL_NAME, method_handler));
    }
}

impl PathProviderPlugin {
    pub fn new() -> Self {
        Self {
            handler: Arc::new(RwLock::new(Handler)),
        }
    }
}

impl Handler {
    fn get_directory_result(&self, dir: Option<PathBuf>) -> Result<Value, MethodCallError> {
        match dir {
            Some(dir) => Ok(Value::String(dir.to_string_lossy().into_owned())),
            None => Err(MethodCallError::UnspecifiedError),
        }
    }

    fn get_temporary_directory(&self) -> Result<Value, MethodCallError> {
        self.get_directory_result(paths::get_cache_dir())
    }

    fn get_application_documents_directory(&self) -> Result<Value, MethodCallError> {
        self.get_directory_result(paths::get_data_dir())
    }

    fn get_storage_directory(&self) -> Result<Value, MethodCallError> {
        self.get_directory_result(paths::get_home_dir())
    }
}

impl MethodCallHandler for Handler {
    fn on_method_call(
        &mut self,
        call: MethodCall,
        _: RuntimeData,
    ) -> Result<Value, MethodCallError> {
        debug!(
            "Got method call {} with args: {}",
            call.method,
            super::debug_print_args(&call.args)
        );
        match call.method.as_str() {
            "getTemporaryDirectory" => self.get_temporary_directory(),
            "getApplicationDocumentsDirectory" => self.get_application_documents_directory(),
            "getStorageDirectory" => self.get_storage_directory(),
            _ => Err(MethodCallError::NotImplemented),
        }
    }
}
