use std::collections::HashMap;
use std::sync::{mpsc::SyncSender, Mutex};

use flutter_engine::{plugins::prelude::*, texture_registry::ExternalTexture};
use log::{debug, info};
use tokio::prelude::Future;

use crate::{util::event_sink::EventSink, PerFrameCallback};

mod ffmpeg_player;

pub const PLUGIN_NAME: &str = module_path!();
pub const CHANNEL_NAME: &str = "flutter.io/videoPlayer";

const DEFAULT_MAX_CACHE_SIZE: i32 = 100 * 1024 * 1024;
const DEFAULT_MAX_FILE_SIZE: i32 = 10 * 1024 * 1024;

pub struct VideoPlayerPlugin {
    handler: Arc<RwLock<Handler>>,
}

struct Handler {
    frame_fn_tx: SyncSender<Box<PerFrameCallback>>,
    players: Mutex<HashMap<i64, Arc<Mutex<VideoPlayer>>>>,
}

struct VideoPlayer {
    handler: Arc<RwLock<VideoPlayerHandler>>,
    event_sink: Arc<Mutex<EventSink>>,
    frame_fn_tx: SyncSender<Box<PerFrameCallback>>,
    texture_name: i64,
    channel_name: String,
    backend: ffmpeg_player::FFmpegPlayer,
}

struct VideoPlayerHandler {
    event_sink: Arc<Mutex<EventSink>>,
}

impl Plugin for VideoPlayerPlugin {
    fn plugin_name() -> &'static str {
        PLUGIN_NAME
    }

    fn init_channels(&mut self, registrar: &mut ChannelRegistrar) {
        let method_handler = Arc::downgrade(&self.handler);
        registrar.register_channel(StandardMethodChannel::new(CHANNEL_NAME, method_handler));
    }
}

impl VideoPlayerPlugin {
    pub fn new(frame_fn_tx: SyncSender<Box<PerFrameCallback>>) -> Self {
        let handler = Arc::new(RwLock::new(Handler {
            frame_fn_tx,
            players: Mutex::new(HashMap::new()),
        }));
        Self { handler }
    }
}

impl VideoPlayer {
    pub fn new(handler: &Handler, texture: Arc<ExternalTexture>, source_uri: String) -> Self {
        let texture_id = texture.handle();
        let channel_name = format!("flutter.io/videoPlayer/videoEvents{}", texture_id);
        let event_sink = Arc::new(Mutex::new(EventSink::new(channel_name.clone())));
        let backend = ffmpeg_player::FFmpegPlayer::new(source_uri.clone(), texture);
        let player = Self {
            handler: Arc::new(RwLock::new(VideoPlayerHandler {
                event_sink: Arc::clone(&event_sink),
            })),
            event_sink,
            frame_fn_tx: handler.frame_fn_tx.clone(),
            texture_name: texture_id,
            channel_name,
            backend,
        };
        let player_handler = Arc::downgrade(&player.handler);
        let channel_name = player.channel_name.clone();
        let (tx, rx) = std::sync::mpsc::channel();
        handler
            .frame_fn_tx
            .send(Box::new(move |window_state| {
                window_state
                    .plugin_registrar
                    .channel_registry
                    .with_channel_registrar(PLUGIN_NAME, |registrar| {
                        registrar.register_channel(EventChannel::new(channel_name, player_handler));
                    });
                tx.send(true).unwrap();
            }))
            .unwrap();
        // wait here until the channel has been created
        rx.recv().unwrap();
        player
    }

    fn init(&mut self, rt: RuntimeData) {
        let backend = &mut self.backend;
        let init = backend.init(rt);
        debug!("video player {} initialised", self.texture_name);

        let mut event = HashMap::new();
        event.insert("event".into(), Value::String("initialized".into()));
        event.insert("duration".into(), Value::I64(init.duration));
        event.insert(
            "width".into(),
            Value::I32(i32::try_from(init.size.0).unwrap()),
        );
        event.insert(
            "height".into(),
            Value::I32(i32::try_from(init.size.1).unwrap()),
        );
        let event_sink = self.event_sink.lock().unwrap();
        event_sink.success(Value::Map(event)).unwrap();
    }

    fn on_method_call(
        &mut self,
        call: MethodCall,
        _rt: RuntimeData,
    ) -> Result<Value, MethodCallError> {
        match call.method.as_str() {
            "setLooping" => {
                let args = from_value::<SetLoopingArgs>(&call.args)?;
                self.backend.set_looping(args.looping);
                Ok(Value::Null)
            }
            "setVolume" => Err(MethodCallError::NotImplemented),
            "pause" => {
                self.backend.pause();
                Ok(Value::Null)
            }
            "play" => {
                self.backend.play();
                Ok(Value::Null)
            }
            "seekTo" => {
                let args = from_value::<SeekToArgs>(&call.args)?;
                self.backend.seek_to(args.location);
                Ok(Value::Null)
            }
            "position" => Ok(Value::I64(self.backend.position())),
            _ => Err(MethodCallError::NotImplemented),
        }
    }
}

impl Drop for VideoPlayer {
    fn drop(&mut self) {
        let channel_name = self.channel_name.clone();
        let texture_name = self.texture_name;
        info!("Dropping video player {}", texture_name);
        self.frame_fn_tx
            .send(Box::new(move |window_state| {
                window_state
                    .plugin_registrar
                    .channel_registry
                    .remove_channel(&channel_name);
            }))
            .unwrap();
    }
}

impl Handler {
    fn init(&mut self) -> Result<Value, MethodCallError> {
        self.players.lock().unwrap().clear();
        Ok(Value::Null)
    }

    fn create(&mut self, args: CreateArgs, rt: RuntimeData) -> Result<Value, MethodCallError> {
        let _max_cache_size = args.max_cache_size.unwrap_or(DEFAULT_MAX_CACHE_SIZE);
        let _max_file_size = args.max_file_size.unwrap_or(DEFAULT_MAX_FILE_SIZE);
        if let Some(_asset) = args.asset {
            // TODO: asset implementation
            Err(MethodCallError::NotImplemented)
        } else if let Some(source_uri) = args.uri {
            // create new OpenGL texture
            let texture = rt.create_external_texture()?;
            let texture_name = texture.handle();
            info!("Creating video player {}", texture_name);
            let player = {
                let player = Arc::new(Mutex::new(VideoPlayer::new(self, texture, source_uri)));
                let mut players = self.players.lock().unwrap();
                players.insert(texture_name, Arc::clone(&player));
                player
            };

            // initialise the player in the background
            let thread_rt = rt.clone();
            rt.task_executor
                .spawn(tokio::prelude::future::ok(()).map(move |_| {
                    let mut player = player.lock().unwrap();
                    player.init(thread_rt);
                }));

            // tell the app the texture id
            Ok(json_value!( {
                "textureId": texture_name,
            }))
        } else {
            Err(MethodCallError::UnspecifiedError)
        }
    }
}

impl MethodCallHandler for Handler {
    fn on_method_call(
        &mut self,
        call: MethodCall,
        rt: RuntimeData,
    ) -> Result<Value, MethodCallError> {
        debug!(
            "Got method call {} with args: {}",
            call.method,
            super::debug_print_args(&call.args)
        );
        match call.method.as_str() {
            "init" => self.init(),
            "create" => self.create(from_value(&call.args)?, rt),
            _ => {
                let texture_id = from_value::<TextureIDArgs>(&call.args)?.texture_id;
                let mut players = self.players.lock().unwrap();
                if call.method.as_str() == "dispose" {
                    players.remove(&texture_id);
                    return Ok(Value::Null);
                }
                match players.get_mut(&texture_id) {
                    Some(player) => player.lock().unwrap().on_method_call(call, rt),
                    None => Err(MethodCallError::CustomError {
                        code: "Unknown textureId".into(),
                        message: format!(
                            "No video player associated with texture id {}",
                            texture_id
                        ),
                        details: Value::Null,
                    }),
                }
            }
        }
    }
}

impl EventHandler for VideoPlayerHandler {
    fn on_listen(&mut self, _args: Value, rt: RuntimeData) -> Result<Value, MethodCallError> {
        debug!("video player on_listen");
        let mut event_sink = self.event_sink.lock().unwrap();
        event_sink.start_listening(rt)?;
        Ok(Value::Null)
    }

    fn on_cancel(&mut self, _rt: RuntimeData) -> Result<Value, MethodCallError> {
        debug!("video player on_cancel");
        let mut event_sink = self.event_sink.lock().unwrap();
        event_sink.stop_listening()?;
        Ok(Value::Null)
    }
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct CreateArgs {
    pub max_cache_size: Option<i32>,
    pub max_file_size: Option<i32>,
    pub asset: Option<String>,
    pub package: Option<String>,
    pub uri: Option<String>,
    pub format_hint: Option<String>,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct TextureIDArgs {
    pub texture_id: i64,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct SetLoopingArgs {
    looping: bool,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct SeekToArgs {
    location: i64,
}
