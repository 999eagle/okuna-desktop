use std::collections::HashMap;
use std::fs::File;

use self::crypto::Crypto;
use crate::util::paths;

use flutter_engine::plugins::prelude::*;
use log::{debug, trace, warn};

mod crypto;

const PLUGIN_NAME: &str = module_path!();
pub const CHANNEL_NAME: &str = "plugins.it_nomads.com/flutter_secure_storage";
const STORAGE_FILE_NAME: &str = "secure_storage.json";

#[derive(Serialize, Deserialize)]
struct ReadArgs {
    pub key: String,
}
#[derive(Serialize, Deserialize)]
struct WriteArgs {
    pub key: String,
    pub value: String,
}
#[derive(Serialize, Deserialize)]
struct DeleteArgs {
    pub key: String,
}

pub struct FlutterSecureStoragePlugin {
    handler: Arc<RwLock<Handler>>,
}

struct Handler {
    storage: HashMap<String, String>,
    crypto: Crypto,
}

impl Plugin for FlutterSecureStoragePlugin {
    fn plugin_name() -> &'static str {
        PLUGIN_NAME
    }

    fn init_channels(&mut self, registrar: &mut ChannelRegistrar) {
        let method_handler = Arc::downgrade(&self.handler);
        registrar.register_channel(StandardMethodChannel::new(CHANNEL_NAME, method_handler));
    }
}

impl FlutterSecureStoragePlugin {
    pub fn new() -> Self {
        let mut storage = match paths::get_data_dir() {
            Some(dir) => match File::open(dir.join(STORAGE_FILE_NAME)) {
                Ok(file) => match serde_json::from_reader(file) {
                    Ok(result) => result,
                    _ => HashMap::new(),
                },
                _ => HashMap::new(),
            },
            None => HashMap::new(),
        };

        let crypto = Crypto::from_storage(&mut storage).expect("Failed to create crypto");

        Handler::save(&storage);

        Self {
            handler: Arc::new(RwLock::new(Handler { storage, crypto })),
        }
    }

    pub fn read_value(&self, key: &str) -> Option<String> {
        self.handler.read().ok().and_then(|h| h.read_value(key))
    }

    pub fn write_value(&mut self, key: String, value: String) {
        self.handler
            .write()
            .map(|mut h| h.write_value(key, value))
            .ok();
    }
}

impl Handler {
    fn save(storage: &HashMap<String, String>) {
        let result = match serde_json::to_string(storage) {
            Ok(data) => match paths::get_data_dir() {
                Some(dir) => std::fs::write(dir.join(STORAGE_FILE_NAME), data).ok(),
                None => None,
            },
            _ => None,
        };
        if result.is_none() {
            warn!("Failed to save");
        }
    }

    pub fn read_value(&self, key: &str) -> Option<String> {
        match self.storage.get(key) {
            Some(v) => match self.crypto.decrypt(v) {
                Ok(data) => Some(data),
                _ => None,
            },
            _ => None,
        }
    }

    pub fn write_value(&mut self, key: String, value: String) {
        if let Ok(data) = self.crypto.encrypt(&value) {
            self.storage.insert(key, data);
            Self::save(&self.storage);
        }
    }

    fn read(&self, args: ReadArgs) -> Result<Value, MethodCallError> {
        trace!("Read key {}", args.key);

        match self.storage.get(&args.key) {
            Some(v) => {
                let data = self
                    .crypto
                    .decrypt(v)
                    .map_err(MethodCallError::from_error)?;
                Ok(Value::String(data))
            }
            None => Ok(Value::Null),
        }
    }

    fn write(&mut self, args: WriteArgs) -> Result<Value, MethodCallError> {
        trace!("Write key {}. New value: {}", args.key, args.value);

        let data = self
            .crypto
            .encrypt(&args.value)
            .map_err(MethodCallError::from_error)?;
        self.storage.insert(args.key, data);
        Self::save(&self.storage);
        Ok(Value::Null)
    }

    fn delete(&mut self, args: DeleteArgs) -> Result<Value, MethodCallError> {
        trace!("Delete key {}", args.key);
        self.storage.remove(&args.key);
        Self::save(&self.storage);
        Ok(Value::Null)
    }

    fn read_all(&self) -> Result<Value, MethodCallError> {
        trace!("Read all");
        let mut map = HashMap::<String, Value>::new();
        for (key, value) in self.storage.iter() {
            if let Ok(data) = self.crypto.decrypt(value) {
                map.insert(key.clone(), Value::String(data));
            }
        }
        Ok(Value::Map(map))
    }

    fn delete_all(&mut self) -> Result<Value, MethodCallError> {
        trace!("Delete all");
        self.storage.clear();
        Ok(Value::Null)
    }

    //    fn decode_value(&self, key: &str) -> Option<String> {
    //        if let Some(value) = self.storage.get(key) {
    //            if let Ok(bytes) = base64::decode(value) {}
    //        }
    //        None
    //    }
}

impl MethodCallHandler for Handler {
    fn on_method_call(
        &mut self,
        call: MethodCall,
        _: RuntimeData,
    ) -> Result<Value, MethodCallError> {
        debug!(
            "Got method call {} with args: {}",
            call.method,
            super::debug_print_args(&call.args)
        );
        match call.method.as_str() {
            "read" => {
                let args = from_value::<ReadArgs>(&call.args)?;
                self.read(args)
            }
            "write" => {
                let args = from_value::<WriteArgs>(&call.args)?;
                self.write(args)
            }
            "delete" => {
                let args = from_value::<DeleteArgs>(&call.args)?;
                self.delete(args)
            }
            "readAll" => self.read_all(),
            "deleteAll" => self.delete_all(),
            _ => Err(MethodCallError::NotImplemented),
        }
    }
}
