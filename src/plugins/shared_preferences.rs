use std::collections::HashMap;
use std::fs::File;

use crate::util::paths;

use flutter_engine::plugins::prelude::*;
use log::{debug, warn};

const PLUGIN_NAME: &str = module_path!();
const CHANNEL_NAME: &str = "plugins.flutter.io/shared_preferences";
const STORAGE_FILE_NAME: &str = "shared_preferences.json";

pub struct SharedPreferencesPlugin {
    handler: Arc<RwLock<Handler>>,
}

struct Handler {
    storage: HashMap<String, Value>,
}

impl Plugin for SharedPreferencesPlugin {
    fn plugin_name() -> &'static str {
        PLUGIN_NAME
    }

    fn init_channels(&mut self, registrar: &mut ChannelRegistrar) {
        let method_handler = Arc::downgrade(&self.handler);
        registrar.register_channel(StandardMethodChannel::new(CHANNEL_NAME, method_handler));
    }
}

impl SharedPreferencesPlugin {
    pub fn new() -> Self {
        let storage = match paths::get_data_dir() {
            Some(dir) => match File::open(dir.join(STORAGE_FILE_NAME)) {
                Ok(file) => match serde_json::from_reader(file) {
                    Ok(result) => result,
                    _ => HashMap::new(),
                },
                _ => HashMap::new(),
            },
            None => HashMap::new(),
        };

        Self {
            handler: Arc::new(RwLock::new(Handler { storage })),
        }
    }

    pub fn get_all(&self) -> HashMap<String, Value> {
        let handler = self.handler.read().unwrap();
        handler.storage.clone()
    }

    pub fn set(&mut self, key: String, value: Value) -> Option<()> {
        let mut handler = self.handler.write().unwrap();
        handler.set(key, value).ok().map(|_| ())
    }
}

impl Handler {
    fn save(&self) -> Result<Value, MethodCallError> {
        let result = match serde_json::to_string(&self.storage) {
            Ok(data) => match paths::get_data_dir() {
                Some(dir) => std::fs::write(dir.join(STORAGE_FILE_NAME), data).ok(),
                None => None,
            },
            _ => None,
        };
        if result.is_none() {
            warn!("Failed to save");
            Ok(Value::Boolean(false))
        } else {
            Ok(Value::Boolean(true))
        }
    }

    fn get_all(&self) -> Result<Value, MethodCallError> {
        let mut storage = HashMap::new();
        for (k, v) in &self.storage {
            if k.starts_with("flutter.") {
                storage.insert(k.clone(), v.clone());
            }
        }
        Ok(Value::Map(storage))
    }

    fn set(&mut self, key: String, value: Value) -> Result<Value, MethodCallError> {
        self.storage.insert(key, value);
        self.save()
    }

    fn remove(&mut self, args: RemoveArgs) -> Result<Value, MethodCallError> {
        self.storage.remove(&args.key);
        self.save()
    }

    fn clear(&mut self) -> Result<Value, MethodCallError> {
        self.storage.clear();
        self.save()
    }
}

impl MethodCallHandler for Handler {
    fn on_method_call(
        &mut self,
        call: MethodCall,
        _: RuntimeData,
    ) -> Result<Value, MethodCallError> {
        debug!(
            "Got method call {} with args: {}",
            call.method,
            super::debug_print_args(&call.args)
        );
        match call.method.as_str() {
            "getAll" => self.get_all(),
            "setBool" => {
                let args = from_value::<SetBoolArgs>(&call.args)?;
                self.set(args.key, Value::Boolean(args.value))
            }
            "setInt" => {
                let args = from_value::<SetIntArgs>(&call.args)?;
                self.set(args.key, Value::I64(args.value))
            }
            "setDouble" => {
                let args = from_value::<SetDoubleArgs>(&call.args)?;
                self.set(args.key, Value::F64(args.value))
            }
            "setString" => {
                let args = from_value::<SetStringArgs>(&call.args)?;
                self.set(args.key, Value::String(args.value))
            }
            "setStringList" => {
                let args = from_value::<SetStringListArgs>(&call.args)?;
                self.set(
                    args.key,
                    Value::List(args.value.into_iter().map(Value::String).collect()),
                )
            }
            "remove" => {
                let args = from_value::<RemoveArgs>(&call.args)?;
                self.remove(args)
            }
            "commit" => self.save(),
            "clear" => self.clear(),
            _ => Err(MethodCallError::NotImplemented),
        }
    }
}

#[derive(Serialize, Deserialize)]
struct SetBoolArgs {
    pub key: String,
    pub value: bool,
}

#[derive(Serialize, Deserialize)]
struct SetIntArgs {
    pub key: String,
    pub value: i64,
}

#[derive(Serialize, Deserialize)]
struct SetDoubleArgs {
    pub key: String,
    pub value: f64,
}

#[derive(Serialize, Deserialize)]
struct SetStringArgs {
    pub key: String,
    pub value: String,
}

#[derive(Serialize, Deserialize)]
struct SetStringListArgs {
    pub key: String,
    pub value: Vec<String>,
}

#[derive(Serialize, Deserialize)]
struct RemoveArgs {
    pub key: String,
}
