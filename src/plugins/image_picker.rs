use std::{fs::File, path::Path, sync::mpsc::SyncSender};

use crate::util;

use flutter_engine::plugins::prelude::*;
use image::GenericImageView;
use log::{debug, info};
use serde_repr::{Deserialize_repr, Serialize_repr};

const PLUGIN_NAME: &str = module_path!();
pub const CHANNEL_NAME: &str = "plugins.flutter.io/image_picker";

#[derive(Serialize_repr, Deserialize_repr)]
#[repr(i32)]
enum ImageSource {
    Gallery = 0,
    Camera = 1,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct PickImageArgs {
    source: Option<ImageSource>,
    max_width: Option<f64>,
    max_height: Option<f64>,
}

pub struct ImagePickerPlugin {
    handler: Arc<RwLock<Handler>>,
}

struct Handler {
    default_path: String,
    path_tx: SyncSender<String>,
}

impl Plugin for ImagePickerPlugin {
    fn plugin_name() -> &'static str {
        PLUGIN_NAME
    }

    fn init_channels(&mut self, registrar: &mut ChannelRegistrar) {
        let method_handler = Arc::downgrade(&self.handler);
        registrar.register_channel(StandardMethodChannel::new(CHANNEL_NAME, method_handler));
    }
}

impl ImagePickerPlugin {
    pub fn new(default_path: String, path_tx: SyncSender<String>) -> Self {
        Self {
            handler: Arc::new(RwLock::new(Handler {
                default_path,
                path_tx,
            })),
        }
    }
}
impl Handler {
    fn pick_image(&mut self, args: PickImageArgs) -> Result<Value, MethodCallError> {
        let filter = Some((&["*.jpg", "*.png", "*.gif", "*.bmp"][0..], "Image files"));
        let result = tinyfiledialogs::open_file_dialog("Okuna", self.default_path.as_str(), filter);
        match result {
            Some(path) => {
                info!("Opening image from {}", path);
                if let Some(path) = Path::new(path.as_str()).parent() {
                    let path = path.to_str().unwrap();
                    debug!("Setting new default path to {}", path);
                    self.default_path = path.to_owned();
                    self.path_tx.send(self.default_path.clone()).unwrap();
                }
                let mut img = image::open(path).map_err(MethodCallError::from_error)?;
                if let Some(cache_path) =
                    util::paths::get_cache_dir().map(|p| p.join("img_upload.jpg"))
                {
                    let mut cache_file =
                        File::create(cache_path.clone()).map_err(MethodCallError::from_error)?;
                    let mut scale = 1.0_f64;
                    let (width, height) = (f64::from(img.width()), f64::from(img.height()));
                    if let Some(max_width) = args.max_width {
                        scale = scale.min(max_width / width);
                    }
                    if let Some(max_height) = args.max_height {
                        scale = scale.min(max_height / height);
                    }
                    if scale < 1.0 {
                        img = img.resize(
                            (width * scale) as u32,
                            (height * scale) as u32,
                            image::Lanczos3,
                        );
                    }
                    if img
                        .write_to(&mut cache_file, image::ImageOutputFormat::JPEG(100))
                        .is_ok()
                    {
                        Ok(Value::String(cache_path.to_string_lossy().into_owned()))
                    } else {
                        // write/encode image failed
                        Err(MethodCallError::UnspecifiedError)
                    }
                } else {
                    // get cache path failed
                    Err(MethodCallError::UnspecifiedError)
                }
            }
            None => Ok(Value::Null),
        }
    }
}

impl MethodCallHandler for Handler {
    fn on_method_call(
        &mut self,
        call: MethodCall,
        _: RuntimeData,
    ) -> Result<Value, MethodCallError> {
        debug!(
            "Got method call {} with args: {}",
            call.method,
            super::debug_print_args(&call.args)
        );

        match call.method.as_str() {
            "pickImage" => {
                let args = from_value::<PickImageArgs>(&call.args)?;
                self.pick_image(args)
            }
            _ => Err(MethodCallError::NotImplemented),
        }
    }
}
