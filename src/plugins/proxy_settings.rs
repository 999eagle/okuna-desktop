use flutter_engine::plugins::prelude::*;
use log::{debug, error, info};
use proxy::error::ProxyConfigError;
use serde::{Deserialize, Serialize};
use url::Url;

use crate::{settings, util::paths};

pub const PLUGIN_NAME: &str = module_path!();
pub const CHANNEL_NAME: &str = "social.okuna/proxy_settings";

#[derive(Serialize, Deserialize)]
struct FindProxyArgs {
    url: String,
}

pub struct ProxySettingsPlugin {
    handler: Arc<RwLock<Handler>>,
}

struct Handler;

impl Plugin for ProxySettingsPlugin {
    fn plugin_name() -> &'static str {
        PLUGIN_NAME
    }

    fn init_channels(&mut self, registrar: &mut ChannelRegistrar) {
        let method_handler = Arc::downgrade(&self.handler);
        registrar.register_channel(StandardMethodChannel::new(CHANNEL_NAME, method_handler));
    }
}

impl ProxySettingsPlugin {
    pub fn new() -> Self {
        Self {
            handler: Arc::new(RwLock::new(Handler)),
        }
    }
}

impl Handler {
    fn format_proxy_url(url: &Url) -> String {
        match url.scheme() {
            "direct" => "DIRECT".into(),
            _ => {
                // flutter needs the proxy in the format
                // PROXY [user:pass@]host:port
                // without any protocol or path and no component may be empty
                let mut cred = match url.password() {
                    Some(pass) => format!("{}:{}", url.username(), pass),
                    None => "".into(),
                };
                if cred.starts_with(':') || cred.ends_with(':') {
                    cred.clear();
                } else if !cred.is_empty() {
                    cred.push('@');
                }

                format!(
                    "PROXY {}{}:{}",
                    cred,
                    url.host_str().unwrap(),
                    // flutter uses 1080 as default port, see `http_impl.dart#L2326`
                    url.port_or_known_default().unwrap_or(1080)
                )
            }
        }
    }

    fn find_proxy(&self, args: FindProxyArgs) -> Result<Value, MethodCallError> {
        let url = url::Url::parse(&args.url).map_err(MethodCallError::from_error)?;
        let settings = settings::get_settings();
        // skip entire proxy set up if a default proxy is specified in app settings
        if let Some(proxy) = settings.proxy_settings().default_proxy() {
            let proxy = if proxy == "DIRECT" {
                proxy.clone()
            } else {
                match Url::parse(proxy.as_str()) {
                    Ok(url) => Handler::format_proxy_url(&url),
                    Err(e) => {
                        error!("Invalid proxy url: {}", e);
                        "DIRECT".into()
                    }
                }
            };
            info!("Using proxy {} for url {}", proxy, url.as_str());
            return Ok(Value::String(proxy));
        }
        let mut factory = proxy::ProxyFactoryBuilder::new()
            .enable_pac(settings.proxy_settings().auto_config_enabled())
            .enable_wpad(settings.proxy_settings().auto_config_enabled())
            .add_default_extensions()
            .build();
        match factory.get_proxies_for_url(&url) {
            Ok(proxies) => {
                let proxy = Handler::format_proxy_url(proxies.first().unwrap());
                info!("Using proxy {} for url {}", proxy, url.as_str());
                Ok(Value::String(proxy))
            }
            Err(err) => {
                error!("proxy configuration error: {}", err);
                match err {
                    ProxyConfigError::WpadNotEnabled | ProxyConfigError::PacNotEnabled => {
                        let settings_file = paths::get_settings_file_path()
                            .to_string_lossy()
                            .to_string();
                        tinyfiledialogs::message_box_ok(
                            "Okuna Desktop",
                            &format!("Your proxy seems to use auto configuration. As this is a potential security risk, this is disabled by default in okuna-desktop. Please enable this manually in the settings file, which you can find here: {}", settings_file),
                            tinyfiledialogs::MessageBoxIcon::Info,
                        );
                    }
                    _ => {}
                }
                Ok(Value::String("DIRECT".into()))
            }
        }
    }
}

impl MethodCallHandler for Handler {
    fn on_method_call(
        &mut self,
        call: MethodCall,
        _: RuntimeData,
    ) -> Result<Value, MethodCallError> {
        debug!(
            "Got method call {} with args: {}",
            call.method,
            super::debug_print_args(&call.args)
        );
        match call.method.as_str() {
            "findProxy" => self.find_proxy(from_value(&call.args)?),
            _ => Err(MethodCallError::NotImplemented),
        }
    }
}
