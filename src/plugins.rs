pub use self::{
    connectivity::ConnectivityPlugin, error_reporting::ErrorReportingPlugin, ffmpeg::FFmpegPlugin,
    file_picker::FilePickerPlugin, flutter_secure_storage::FlutterSecureStoragePlugin,
    get_version::GetVersionPlugin, image_compress::ImageCompressPlugin,
    image_converter::ImageConverterPlugin, image_cropper::ImageCropperPlugin,
    image_picker::ImagePickerPlugin, multi_image_picker::MultiImagePickerPlugin,
    package_info::PackageInfoPlugin, path_provider::PathProviderPlugin,
    proxy_settings::ProxySettingsPlugin, shared_preferences::SharedPreferencesPlugin,
    sqflite::SqflitePlugin, url_launcher::UrlLauncherPlugin, video_player::VideoPlayerPlugin,
    video_thumbnail::VideoThumbnailPlugin,
};

use flutter_engine::codec::Value;

pub mod connectivity;
pub mod error_reporting;
pub mod ffmpeg;
pub mod file_picker;
pub mod flutter_secure_storage;
pub mod get_version;
pub mod image_compress;
pub mod image_converter;
pub mod image_cropper;
pub mod image_picker;
pub mod multi_image_picker;
pub mod package_info;
pub mod path_provider;
pub mod proxy_settings;
pub mod shared_preferences;
pub mod sqflite;
pub mod url_launcher;
pub mod video_player;
pub mod video_thumbnail;

fn debug_print_args(value: &Value) -> String {
    match value {
        Value::String(string) => format!("String: {}", string),
        Value::Boolean(bool) => format!("Boolean: {}", bool),
        Value::F64(num) => format!("F64: {}", num),
        Value::I32(num) => format!("I32: {}", num),
        Value::I64(num) => format!("I64: {}", num),
        Value::Null => "Null".into(),
        Value::F64List(_list) => "F64List".into(),
        Value::I32List(_list) => "I32List".into(),
        Value::I64List(_list) => "I64List".into(),
        Value::U8List(_list) => "U8List".into(),
        Value::List(list) => {
            let mut string = String::from("List: {\n");
            for value in list.iter() {
                string += format!("\t{}\n", &debug_print_args(value)).as_str();
            }
            string + "}"
        }
        Value::Map(map) => {
            let mut string = String::from("Map: {\n");
            for (key, value) in map.iter() {
                string += format!("\t{}:\n\t{}\n", key, &debug_print_args(value)).as_str();
            }
            string + "}"
        }
    }
}
