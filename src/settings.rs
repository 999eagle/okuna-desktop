use std::io::Read;
use std::{fs, path::Path, sync::Arc};

use log::{info, warn};
use serde::{Deserialize, Serialize};

use lazy_static::lazy_static;

use crate::util::paths;

lazy_static! {
    static ref SETTINGS: Arc<Settings> = Arc::new(init_settings());
}

#[derive(Deserialize, Default, Clone, Serialize)]
pub struct Settings {
    #[serde(default)]
    proxy: Proxy,
}

#[derive(Deserialize, Default, Clone, Serialize)]
pub struct Proxy {
    #[serde(default)]
    enable_auto_config: bool,
    force_proxy: Option<String>,
}

impl Settings {
    pub fn proxy_settings(&self) -> &Proxy {
        &self.proxy
    }
}

impl Proxy {
    pub fn auto_config_enabled(&self) -> bool {
        self.enable_auto_config
    }

    pub fn default_proxy(&self) -> &Option<String> {
        &self.force_proxy
    }
}

fn read_settings_from_file<P: AsRef<Path>>(file: P) -> Result<Settings, SettingsError> {
    if !file.as_ref().is_file() {
        return Err(SettingsError::NoSettingsFile);
    }
    let mut f = fs::File::open(file)?;
    let mut contents = String::new();
    f.read_to_string(&mut contents)?;
    Ok(toml::from_str(&contents)?)
}

fn create_default_settings_file() -> Result<(), SettingsError> {
    let example_file = paths::get_assets_dir().join("settings.example.toml");
    let settings_file = paths::get_settings_file_path();
    if example_file.is_file() {
        fs::copy(example_file, settings_file)?;
    } else {
        warn!("example file not found! serializing empty settings");
        let settings = Settings::default();
        let serialized = toml::to_string(&settings)?;
        fs::write(settings_file, serialized)?;
    }

    Ok(())
}

fn init_settings() -> Settings {
    let path = paths::get_settings_file_path();
    match read_settings_from_file(&path) {
        Ok(settings) => {
            info!("loaded settings from {}", path.to_string_lossy());
            settings
        }
        Err(SettingsError::NoSettingsFile) => {
            info!(
                "no settings file, creating default at {}",
                path.to_string_lossy()
            );
            if let Err(err) = create_default_settings_file() {
                warn!("failed to create default settings file: {}", err);
            }
            Default::default()
        }
        Err(err) => {
            warn!("failed to read settings, using defaults: {}", err);
            Default::default()
        }
    }
}

pub fn get_settings() -> Arc<Settings> {
    Arc::clone(&SETTINGS)
}

enum SettingsError {
    NoSettingsFile,
    IoError(std::io::Error),
    SerializeError(toml::ser::Error),
    ParseError(toml::de::Error),
}

impl From<std::io::Error> for SettingsError {
    fn from(e: std::io::Error) -> Self {
        SettingsError::IoError(e)
    }
}

impl From<toml::ser::Error> for SettingsError {
    fn from(e: toml::ser::Error) -> Self {
        SettingsError::SerializeError(e)
    }
}

impl From<toml::de::Error> for SettingsError {
    fn from(e: toml::de::Error) -> Self {
        SettingsError::ParseError(e)
    }
}

impl std::fmt::Display for SettingsError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            SettingsError::IoError(err) => write!(f, "io error: {}", err),
            SettingsError::SerializeError(err) => write!(f, "failed to serialize: {}", err),
            SettingsError::ParseError(err) => write!(f, "failed to deserialize: {}", err),
            SettingsError::NoSettingsFile => write!(f, "settings file not found"),
        }
    }
}
