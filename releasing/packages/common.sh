#!/usr/bin/env bash

job_id=$(curl -SsLg --header "PRIVATE-TOKEN: $API_ACCESS_TOKEN" "$CI_API_V4_URL/projects/$CI_PROJECT_ID/pipelines/$CI_PIPELINE_ID/jobs?scope[]=success" |
	jq 'map(select(.name == "release-linux")) | .[0].id')

if [[ -z "$CI_COMMIT_TAG" ]]; then
	version=$(git describe --long --tags | sed 's/\([^-]*-g\)/r\1/;s/-/./g')
	build="bin-git"
else
	version=$(echo "$CI_COMMIT_TAG" | sed 's/-/./g')
	build="bin"
fi

echo "packaging version $version from job id $job_id as $build"
