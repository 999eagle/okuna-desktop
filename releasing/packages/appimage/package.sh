#!/usr/bin/env bash

root_dir=$(pwd)
cd "$(dirname "$0")"
source ../common.sh

pkgname="okuna-desktop${build#bin}"
pkgrel=1

echo "downloading appimagetool"
curl -SsLo "appimagetool" "https://github.com/AppImage/AppImageKit/releases/download/continuous/appimagetool-x86_64.AppImage"
chmod +x appimagetool

echo "building package ${pkgname} ${version}-${pkgrel}"

contains_element() {
	local e match="$1"
	shift
	for e; do [[ "$e" == "$match" ]] && return 0; done
	return 1
}

mkdir -p "AppDir/usr/lib"
mkdir -p "AppDir/usr/share/applications"
mkdir -p "AppDir/opt"

echo "copying files"

# copy main okuna files
mv "${root_dir}/okuna-desktop" "AppDir/opt/"

# set up appimage entry point
cp "okuna-desktop.sh" "AppDir/opt/okuna-desktop/okuna-desktop.sh"
ln -s "opt/okuna-desktop/okuna-desktop.sh" "AppDir/AppRun"

# set up desktop entry
mv "AppDir/opt/okuna-desktop/okuna-desktop.desktop" "AppDir/usr/share/applications"
sed -i 's/^Exec=.*$/Exec=opt\/okuna-desktop\/okuna-desktop/' "AppDir/usr/share/applications/okuna-desktop.desktop"
ln -s "usr/share/applications/okuna-desktop.desktop" "AppDir/okuna-desktop.desktop"

# set up icons
for size in 32 64 256; do
	path="AppDir/usr/share/icons/hicolor/${size}x${size}/apps"
	mkdir -p "${path}"
	ln -s "../../../../../../opt/okuna-desktop/assets/okuna-o-logo_transparent_${size}.png" "${path}/okuna-desktop.png"
done
ln -s "usr/share/icons/hicolor/256x256/apps/okuna-desktop.png" "AppDir/okuna-desktop.png"

# set up libraries
echo "setting up dependencies"
lib_blacklist=('libm' 'libz' 'ld-linux-x86-64' 'libc' 'libgcc_s' 'libpthread' 'libX11' 'libstdc++' 'libdl' 'librt' 'libp11-kit' 'libdrm' 'libxcb' 'libflutter_engine')
patchelf --set-rpath "\$ORIGIN/../../usr/lib" "AppDir/opt/okuna-desktop/okuna-desktop"
strip -s "AppDir/opt/okuna-desktop/okuna-desktop" || true
patchelf --set-rpath "\$ORIGIN/../../usr/lib" "AppDir/opt/okuna-desktop/ffmpeg"
strip -s "AppDir/opt/okuna-desktop/ffmpeg" || true
mv "AppDir/opt/okuna-desktop/libflutter_engine.so" "AppDir/usr/lib"
# copy libs for okuna-desktop
libs=('libflutter_engine.so')
added=('AppDir/opt/okuna-desktop/okuna-desktop' 'AppDir/opt/okuna-desktop/ffmpeg' 'AppDir/usr/lib/libflutter_engine.so')

while [[ ${#added[@]} -ne 0 ]]; do
	to_check=("${added[@]}")
	added=()
	for bin in "${to_check[@]}"; do
		echo "checking dependencies for ${bin}"
		for dep in $(patchelf --print-needed "${bin}"); do
			contains_element "${dep}" "${libs[@]}" && continue
			dep_no_version=$(echo "${dep}" | grep -o "^[^.]*")
			contains_element "${dep_no_version}" "${lib_blacklist[@]}"
			if [[ $? -eq 0 ]]; then
				libs+=("${dep}")
				echo "  skipping blacklisted ${dep}" && continue
			fi
			echo "  copying lib ${dep}"
			found_lib=0
			for path in '/lib' '/usr/lib' '/lib/x86_64-linux-gnu' '/usr/lib/x86_64-linux-gnu'; do
				if [[ -e "${path}/${dep}" ]]; then
					cp "${path}/${dep}" "AppDir/usr/lib"
					found_lib=1
					break
				fi
			done
			if [[ ${found_lib} -eq 0 ]]; then
				echo "  lib ${dep} missing!"
				exit 1
			fi
			libs+=("${dep}")
			added+=("AppDir/usr/lib/${dep}")
		done
	done
done

echo "setting rpaths and stripping libs"
for file in AppDir/usr/lib/*; do
	patchelf --set-rpath "\$ORIGIN" "$file"
	strip -s "$file" || true
done

echo "building AppImage"
./appimagetool AppDir OkunaDesktop-x86_64.AppImage
