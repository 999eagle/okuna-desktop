#!/usr/bin/env sh

current_dir="$(dirname "$0")"
description="$(awk -f "$current_dir/changelog.awk" "CHANGELOG.md" | sed 's/"/\\"/g' | sed ':a;N;$!ba;s/\n/\\n/g')"

jobs=$(curl -SsL \
	--header 'Content_Type: application/json' \
	--header "PRIVATE-TOKEN: $API_ACCESS_TOKEN" \
	"$CI_API_V4_URL/projects/$CI_PROJECT_ID/pipelines/$CI_PIPELINE_ID/jobs?scope[]=success")
app_image_job_id=$(echo "$jobs" | jq 'map(select(.name == "package-appimage")) | .[0].id')
linux_job_id=$(echo "$jobs" | jq 'map(select(.name == "release-linux")) | .[0].id')
macos_job_id=$(echo "$jobs" | jq 'map(select(.name == "release-macos")) | .[0].id')
windows_job_id=$(echo "$jobs" | jq 'map(select(.name == "release-windows")) | .[0].id')

check_null() {
	if [[ "$1" == "null" ]]; then
		echo "Job ID for $2 is null!"
		exit 1
	fi
}

check_null "${app_image_job_id}" 'AppImage'
check_null "${linux_job_id}" 'Linux'
check_null "${macos_job_id}" 'macOS'
check_null "${windows_job_id}" 'Windows'

read -r -d '' PAYLOAD <<EOF
{ "name": "Release $CI_COMMIT_TAG", "tag_name": "$CI_COMMIT_TAG", "description": "$description", "assets": { "links": [
{ "name": "Linux AppImage", "url": "$CI_PROJECT_URL/-/jobs/${app_image_job_id}/artifacts/raw/packages/OkunaDesktop-x86_64.AppImage" },
{ "name": "Linux binaries", "url": "$CI_PROJECT_URL/-/jobs/${linux_job_id}/artifacts/download" },
{ "name": "macOS binaries", "url": "$CI_PROJECT_URL/-/jobs/${macos_job_id}/artifacts/download" },
{ "name": "Windows binaries", "url": "$CI_PROJECT_URL/-/jobs/${windows_job_id}/artifacts/download" }
]}}
EOF
curl -SsL \
	--header 'Content-Type: application/json' \
	--header "PRIVATE-TOKEN: $API_ACCESS_TOKEN" \
	--data "$PAYLOAD" \
	--request POST \
	"$CI_API_V4_URL/projects/$CI_PROJECT_ID/releases"
